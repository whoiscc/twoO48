// twoO48/src/lodash.js
// Extract used part of lodash library.
// Created by Correctizer on Aug 26, 2018.
import map from "lodash/map";
import partition from "lodash/partition";
import concat from "lodash/concat";
import find from "lodash/find";
import filter from "lodash/filter";
import has from "lodash/has";
import each from "lodash/each";
import uniqueId from "lodash/uniqueId";

export default { map, partition, concat, find, filter, has, each, uniqueId };
