// twoO48/src/index.js
// Created with this project, cleaned at Aug 27, 2018.
import { mount } from "./framework";
import view from "./app/view";

import "./style.scss";

const containerElement = document.createElement("div");
document.body.append(containerElement);
mount(view.pause, containerElement);
