// twoO48/src/framework/simple-context.js
// Create by Correctizer on Aug 31, 2018.
import Kefir from "kefir";

function simpleContext2(deferredPromisedConsumer) {
  function stream(expectUpdate) {
    return Kefir.stream(emitter => {
      expectUpdate(element => {
        emitter.emit(element);
      });
    });
  }
  return new Promise((resolve, reject) => {
    stream(update => resolve(update)).observe({
      value(value) {
        deferredPromisedConsumer().then(consumer => consumer(value));
      }
    });
  });
}

export default function simpleContext3() {
  return new Promise((resolveContext, rejectContext) => {
    const promisedConsumer = new Promise((resolve, reject) => {
      simpleContext2(() => promisedConsumer).then(update => {
        const stream = Kefir.stream(emitter => {
          resolve(value => emitter.emit(value));
        });
        resolveContext({ stream, update });
      });
    });
  });
}
