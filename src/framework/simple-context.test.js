// Created: 2018.8.30
import "chai/register-should";
import simpleContext from "./simple-context";

it("_simpleContext yields values which it receives", () => {
  return simpleContext()
    .then(context => {
      context.update(1);
      context.update(2);
      context.update(3);
      return context.stream;
    })
    .then(
      stream =>
        new Promise((resolve, reject) => {
          stream.bufferWithCount(3).observe({
            value(result) {
              resolve(result);
            }
          });
        })
    )
    .then(result => result.should.deep.equal([1, 2, 3]));
});
