import { render } from "react-dom";
import { context, extend } from "./context";

export function mount(view, parentElement) {
  context().then(context => {
    view(context).observe({
      value(frame) {
        render(frame, parentElement);
      }
    });
  });
}

export { extend };
