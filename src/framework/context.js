import Kefir from "kefir";
import simpleContext from "./simple-context";

export function extend(context) {
  return {
    event(view, eventName) {
      return Kefir.stream(emitter => {
        const listener = event => {
          emitter.emit(event);
        };
        context
          .element(view)
          .diff(null, null)
          .observe({
            value([oldElement, newElement]) {
              if (oldElement != null) {
                oldElement.removeEventListener(eventName, listener);
              }
              if (newElement != null) {
                newElement.addEventListener(eventName, listener);
              }
            }
          });
      });
    }
  };
}

export function context() {
  return simpleContext().then(context => ({
    update(view, element) {
      context.update({ view, element });
    },
    element(view) {
      return context.stream
        .filter(bundle => bundle.view == view)
        .map(bundle => bundle.element);
    }
  }));
}
