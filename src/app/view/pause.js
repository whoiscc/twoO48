import { property, React } from "./common";

export default function pauseView(context) {
  return property.isPaused(context).map(isPaused => (
    <button
      className="button"
      ref={element => context.update(pauseView, element)}
    >
      {isPaused ? "Resume" : "Pause"}
    </button>
  ));
}
