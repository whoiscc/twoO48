import { view, extend } from "./common";

export default function isPausedProperty(context) {
  return extend(context)
    .event(view.pause, "click")
    .scan((prev, _event) => !prev, false);
}
