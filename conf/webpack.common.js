const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CleanWebpackPlugin = require("clean-webpack-plugin");

const projectRoot = path.resolve(__dirname, "..");
const distDirectory = path.resolve(projectRoot, "dist");

module.exports = {
  entry: "./src/index.js",
  plugins: [
    new CleanWebpackPlugin([distDirectory], {
      root: projectRoot
    }),
    new HtmlWebpackPlugin({
      title: "2o48 - A More or Less 2048 Game",
      meta: { viewport: "width=device-width, initial-scale=1" },
      favicon: "./assets/icon.png"
    })
  ],
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ["@babel/preset-env", "@babel/preset-react"],
            plugins: ["@babel/plugin-syntax-dynamic-import"]
          }
        }
      },
      {
        test: /\.scss$/,
        use: [
          {
            loader: "style-loader"
          },
          {
            loader: "css-loader",
            options: {
              sourceMap: true
            }
          },
          {
            loader: "sass-loader",
            options: {
              sourceMap: true
            }
          }
        ]
      },
      {
        test: /\.(png|jpg|gif|svg|eot|ttf|woff|woff2)$/,
        loader: "url-loader",
        options: {
          limit: 10000
        }
      }
    ]
  },
  output: {
    path: distDirectory
  }
};
